import React from 'react'
import { useSelector }  from 'react-redux'
import LoaderTempleate from './loader.template'
function PopupLoader() {
    const auth = useSelector((state) => state.auth)
    return (
        <>
            {auth.loading ? <LoaderTempleate /> : null}
        </>
    )
}

export default PopupLoader
