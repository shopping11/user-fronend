import React, { useEffect, useState } from "react";
import { CategoryList } from "./categoryMenu.style";
import { useSelector } from "react-redux";
import { isMobile } from "react-device-detect";
function CategoryMenu() {
  const category = useSelector((state) => state.category);

  const handleCategoryPick = (name) => { 
   window.location.href= `/product/${name}?page=1&search=`
  }

  const renderCatgegory = (category) => {
    const mycategory = [];
    for (let cat of category) {
      mycategory.push(
        <li key={cat.id} >
          {cat.children.length > 0 ? (
            <span onClick={(() => handleCategoryPick(cat.name))}>{cat.name}</span>
          ) : (
            <span onClick={(() => handleCategoryPick(cat.name))}>{cat.name}</span>
          )}
          {cat.children.length > 0 ? (
            <ul>{renderCatgegory(cat.children)}</ul>
          ) : null}
        </li>
      );
    }
    return mycategory;
  };

  return (
    <>
      {!isMobile ? (
        <CategoryList>
          <div className="menu-category">
            <ul>{renderCatgegory(category.categories)}</ul>
          </div>
        </CategoryList>
      ) : null}
    </>
  );
}

export default CategoryMenu;
