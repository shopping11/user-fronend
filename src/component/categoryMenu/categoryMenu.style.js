import styled from "styled-components";

// export const CategoryList = styled.div`
//   display: grid;
//   grid-template-columns: 1fr;
//   grid-auto-rows: 55px;
//   border-bottom: 1px solid #aaaaaa;
//   box-shadow: 0px 2px 2px #aaaaaa;
//   align-items: center;
//   background: white;

//   .menu-category {
//     display: flex;
//     /* margin: 0 30px; */
//   }

//   .menu-category ul {
//     list-style: none;
//     margin: 0;
//     padding: 0;
//     justify-content: center;
//     a {
//       text-decoration: none;
//     }
//   }

//   .menu-category > ul {
//     display: flex;
//     margin: 0 50px;
//     position: relative;
//     width: 100%;

//     /* justify-content: center; */
//     @media screen and (max-width: 460px) {
//       margin: 0 0;
//     }
//   }
//   .menu-category > ul > li > span {
//     display: block;
//     line-height: 40px;
//     cursor: pointer;
//     padding: 0 20px;
//     font-size: 14px;
//     font-weight: bold;

//     /* justify-content: center; */
//     @media screen and (max-width: 460px) {
//       padding: 0 10px;
//     }
//   }

//   .menu-category > ul > li > span:hover {
//     color: rgb(245, 125, 12);
//   }

//   .menu-category > ul > li > ul {
//     position: absolute;
//     background: #fff;
//     left: 20%;
//     right: 20%;
//     border: 1px solid #cecece;
//     display: none;
//     /* justify-content: center; */
//   }

//   .menu-category > ul > li:hover > ul {
//     display: block;
//   }

//   .menu-category > ul > li > ul > li {
//     margin: 0 20px;

//   }

//   .menu-category > ul > li > ul > li {
//     float: left;
//   }
// `;

export const CategoryList = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  grid-auto-rows: 55px;
  border-bottom: 1px solid #aaaaaa;
  box-shadow: 0px 2px 2px #aaaaaa;
  align-items: center;
  background: white;

  .menu-category:after {
    content: "";
    clear: both;
    display: block;
  }
  .menu-category ul {
    list-style: none;
    margin: 0;
    display: flex;
    justify-content: center;
  }

  .menu-category > ul > li {
    /* float: left; */
    position: relative;
  }

  .menu-category ul li span {
    display: block;
    padding: 0px 30px;
    line-height: 50px;
    cursor: pointer;
  }
  .menu-category ul li:hover {
    background: rgb(224, 221, 224);
  }
  .menu-category ul ul {
    position: absolute;
    padding: 0;
    min-width: 160px;
    display: none;
    top: 100%;
    left: 0;
    background: #ffff;
    box-shadow: 0px 0px 2px 1px #aaaaaa;
  }
  .menu-category ul li:hover > ul {
    display: block;
  }
  /* .menu-category ul ul li:hover span {
    background: #262626;
  } */
  .menu-category ul ul li {
    position: relative;
  }
  .menu-category ul ul ul {
    top: 0;
    left: 100%;
  }
  .menu-category ul ul li:hover {
    background: rgb(224, 221, 224);
  }

  .menu-category ul ul ul li:hover span {
    background: rgb(224, 221, 224);
  }
  @media screen and (max-width: 720px) {
    display: none;
  }
`;
