import React from "react";
import { Cards, Container } from "./test.style";
function Test() {
  return (
    <Container>
      <Cards>
        <div className="card">
          <div className="card__image">
            <img src="/img/1.jpg" />
          </div>
          <div className="card_content">
            <p>test</p>
            <p>mart</p>
          </div>
        </div>
        <div className="card">
          <div className="card__image">
            <img src="/img/1.jpg" />
          </div>
          <div className="card_content">
            <p>test</p>
            <p>mart</p>
          </div>
        </div>
        <div className="card">
          <div className="card__image">
            <img src="/img/1.jpg" />
          </div>
          <div className="card_content">
            <p>test</p>
            <p>mart</p>
          </div>
        </div>
        <div className="card">
          <div className="card__image">
            <img src="/img/1.jpg" />
          </div>
          <div className="card_content">
            <p>test</p>
            <p>mart</p>
          </div>
        </div>
        <div className="card">
          <div className="card__image">
            <img src="/img/1.jpg" />
          </div>
          <div className="card_content">
            <p>test</p>
            <p>mart</p>
          </div>
        </div>
      </Cards>
    </Container>
  );
}

export default Test;
