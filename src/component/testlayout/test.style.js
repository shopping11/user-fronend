import styled from "styled-components";

export const Container = styled.div`
  margin: 20px 150px;
  display: grid;
  overflow-x: hidden;
  /* background: red; */
  place-items:center;

  @media screen and (max-width: 730px) {
    margin: 20px 0px;
  }
`;

export const Cards = styled.div`
  margin: 10px 2px;
  max-width: 1000px;
  display: grid;
  grid-template-columns: repeat(3, 40%);
  grid-auto-rows: auto;
  gap: 20px;
  font-family: sans-serif;
  padding-top: 30px;
  

  .card {
    display: grid;
    box-shadow: 0px 2px 2px #aaaaaa;
    grid-template-columns: 1fr;
    grid-template-rows: 250px 100px 50px;
    cursor: pointer;
  }

  img {
    float: left;
    height: 100%;
    width: 100%;
    object-fit: cover;
  }

  .card__content {
    line-height: 1.5;
    font-size: 0.9em;
    padding: 15px;
    background: #fafafa;
  }
`;
