import React, { useState, useEffect, useRef } from "react";
import { NavbarHeader } from "./header.style";
import { BsPerson } from "react-icons/bs";
import { AiOutlineClose } from "react-icons/ai";
import { Link } from "react-router-dom";
import { useSelector, useDispatch } from "react-redux";
import { signout } from "../../action/auth/authAction";
import PopupLoader from "../popupLoader/popUploader";
import { FiShoppingCart } from "react-icons/fi";

// import Paper from "@mui/material/Paper";
// import InputBase from "@mui/material/InputBase";
// import IconButton from "@mui/material/IconButton";
// import SearchIcon from "@mui/icons-material/Search";
import Search from "./search";
import { GetCart } from "../../action/cart/cartAction";

function Header(props) {
  const auth = useSelector((state) => state.auth);
  const cart = useSelector((state) => state.cart);
  const [userOption, setUserOption] = useState(false);
  const [userOption2, setUserOption2] = useState(false);
  let menuUser = useRef();
  let sideBarHidden = useRef();
  const dispatch = useDispatch();

  useEffect(() => {
    document.addEventListener("mousedown", (event) => {
      // console.log(menuUser.current,event.target)
      if (menuUser.current && !menuUser.current.contains(event.target)) {
        // console.log(menuUser.current,event.target)
        setUserOption(false);
      }
    });
  }, []);

  useEffect(() => {
    if (auth.authenticate) {
      dispatch(GetCart());
    }
  }, [auth.authenticate]);

  const handleOption2Open = () => {
    setUserOption2(true);
    // document.addEventListener('touchmove', function(e) { e.preventDefault(); }, { passive:false });
    document.body.style.overflow = "hidden";
    document.addEventListener(
      "touchmove",
      (e) => {
        if (sideBarHidden.current && sideBarHidden.current.contains(e.target)) {
          e.preventDefault();
        }
      },
      { passive: false }
    );
  };

  const handleOption2Close = () => {
    setUserOption2(false);
    document.body.style.overflow = "unset";
  };

  const HandleLogOut = () => {
    dispatch(signout());
    setUserOption2(false);
  };

  return (
    <>
      <PopupLoader />
      <NavbarHeader>
        <div className="NavbarCol1">
          <div className="logo_thumbnail">
            <img src="/img/logo.png" />
          </div>
        </div>
        <div className="NavbarCol2">
          <Search {...props} />
        </div>
        <div className="navbarCol3">
          <Link to="/cart">
            <div className="cart">
              <FiShoppingCart size={25} />{" "}
              <span className="countItem">
                {cart.cart.cartItems?.length || 0}
              </span>
            </div>
          </Link>
          <div
            className="header_username"
            ref={menuUser}
            onClick={
              auth.authenticate ? () => setUserOption(!userOption) : null
            }
          >
            {auth.authenticate ? (
              <div className="col1">{auth.user.username}</div>
            ) : (
              <Link to="/signin">
                <div className="col2">ล็อกอินเข้าระบบ / สมัครสมาชิก</div>
              </Link>
            )}
            {userOption ? (
              <div className="pop_user_option">
                <div className="menu">1</div>
                <div className="menu">
                  <Link to="/order_history?state=pending">
                    การสั่งจองของฉัน
                  </Link>
                </div>
                <div className="menu" onClick={HandleLogOut}>
                  ออกจากระบบ
                </div>
              </div>
            ) : null}
          </div>
          <div className="header_username_hidden">
            <BsPerson
              style={{ cursor: "pointer" }}
              size="25px"
              onClick={handleOption2Open}
            />
            <div
              className={
                userOption2
                  ? "hidden_user_option_open"
                  : "hidden_user_option_hide "
              }
              ref={sideBarHidden}
            >
              <div className="header">
                <AiOutlineClose
                  onClick={handleOption2Close}
                  style={{ marginRight: "5px", cursor: "pointer" }}
                  size="25px"
                />
              </div>
              {auth.authenticate ? (
                <>
                  <div className="content-header">
                    <BsPerson style={{ padding: "20px" }} size="30px" />{" "}
                    {auth.user.username}
                  </div>
                  <div className="content" onClick={HandleLogOut}>
                    <span className="logout">ออกจากระบบ</span>
                  </div>
                </>
              ) : (
                <div className="content">
                  <Link to="/signin">
                    <span className="hidden_login_button">
                      ล็อกอินเข้าระบบ / สมัครสมาชิก
                    </span>
                  </Link>
                </div>
              )}
            </div>
          </div>
        </div>
      </NavbarHeader>
    </>
  );
}

export default Header;
