import React from 'react'
import Paper from "@mui/material/Paper";
import InputBase from "@mui/material/InputBase";
import IconButton from "@mui/material/IconButton";
import SearchIcon from "@mui/icons-material/Search";

function Search(props) {
    return (
        <>
          {props.searchBar ? (
            <Paper
              sx={{
                p: "2px 4px",
                display: "flex",
                alignItems: "center",
                width: "50%",
              }}
            >
              <InputBase
                sx={{ ml: 1, flex: 1 }}
                placeholder="Search"
                value={props.value}
                onChange={props.onChange}
                onKeyPress={props.onKeyPress}
              />
              <IconButton onClick={props.onClick}>
                <SearchIcon />
              </IconButton>
            </Paper>
          ) : null}   
        </>
    )
}

export default Search
