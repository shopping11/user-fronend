import styled from "styled-components";

export const NavbarHeader = styled.div`
  display: grid;
  grid-template-columns: 1fr 3fr 1fr;
  grid-auto-rows: 75px;
  z-index: 998;
  background-image: linear-gradient(
    109.6deg,
    rgba(11, 133, 234, 1) 11.2%,
    rgba(34, 63, 201, 1) 91.1%
  );
  box-shadow: 0px 2px 10px #aaaaaa;

  /* @media screen and (max-width: 1250px) {
    grid-template-columns: 1fr 3fr 2fr;
  } */
  .NavbarCol1 {
    display: grid;
    place-items: center;
  }

  .logo_thumbnail {
    width:  100px;
    height: 80px;
  }

  .logo_thumbnail > img{
    max-width: 100%;
    max-height: 100%;
  }

  .NavbarCol2 {
    display: inline-grid;
    place-items: center;
  }

  .navbarCol3 {
    /* background: red; */
    display: flex;
    place-items: center;
    .cart {
      cursor: pointer;
      color: rgb(255, 255, 255);
      display: flex;
      .countItem {
        position: absolute;
        background: #e0166a;
        border-radius: 50px;
        width: 20px;
        height: 20px;
        font-size: 12px;
        line-height: 20px;
        text-align: center;
        top: 8px;
        color: #fff;
        margin-left: 20px;
      }
    }
    .header_username {
      margin-left: 20px;
      padding: 2% 5%;
      background-color: #123c8c;
      border-radius: 10px;
      color: rgb(255, 255, 255);
      cursor: pointer;
      width: minmax(auto, 70%);
      display: flex;

      a {
        text-decoration: none;
        color: rgb(255, 255, 255);
      }

      .pop_user_option {
        position: absolute;
        /* position: fixed;
        margin-top: 35px;
        margin-left: -20px;
        width: 300px;
        height: 100px; */
        right: 25px;
        top: 70px;
        width: 300px;
        height: 100px;
        background-color: rgb(255, 255, 255);
        -webkit-box-shadow: 0 0 1px #000;
        box-shadow: 0 0 1px #000;
        color: black;
        /* border: solid #000;
        border-width:1px; */
        display: grid;
        grid-template-columns: 1fr;
        .menu {
          padding: 1px;
        }
        .menu a {
          text-decoration: none;
          color: black;
        }
        .menu:hover {
          background-color: lightgray;
        }
      }
      .pop_user_option::before {
        content: "";
        position: absolute;
        height: 0px;
        left: 150px;
        top: -30px;
        border-width: 15px;
        border-color: transparent transparent white;
        border-style: solid;
      }
    }

    .header_username_hidden {
      display: none;
      margin-left: 60px;
    }

    /* @media screen and (max-width: 720px) { */
    @media screen and (max-width: 1250px) {
      .header_username {
        display: none;
      }
      .header_username_hidden {
        display: inline-grid;
        place-items: center;
        color: rgb(255, 255, 255);
      }
      .hidden_user_option_hide {
        transform: translateX(-155%);
        transition: all 0.2s ease;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgb(255, 255, 255);
        display: grid;
        grid-template-columns: 1fr;
        grid-auto-rows: 50px;
        .header {
          display: inline-grid;
          place-items: center end;
          background-image: linear-gradient(
            109.6deg,
            rgba(11, 133, 234, 1) 11.2%,
            rgba(34, 63, 201, 1) 91.1%
          );
        }
        .content {
          color: black;
        }
      }
      .hidden_user_option_open {
        z-index: 999;
        transition: all 0.2s ease;
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background-color: rgb(255, 255, 255);
        display: grid;
        grid-template-columns: 1fr;
        grid-auto-rows: 50px;
        gap: 10px;
        overflow-y: scroll;

        .header {
          display: inline-grid;
          place-items: center end;
          background-image: linear-gradient(
            109.6deg,
            rgba(11, 133, 234, 1) 11.2%,
            rgba(34, 63, 201, 1) 91.1%
          );
        }
        .content {
          color: black;
          display: grid;
          justify-content: start;
          align-items: center;
          padding: 10px;
        }
        .content-header {
          display: flex;
          place-items: center start;
          color: black;
          border-bottom: 1px solid;
        }
        .logout {
          cursor: pointer;
        }
        a {
          text-decoration: none;
        }

        .hidden_login_button {
          display: inline-grid;
          place-items: center;
          width: minmax(auto, 70%);
          background: red;
          border-radius: 10px;
          padding: 10px 20px;
          cursor: pointer;
          background-color: #123c8c;
          color: rgb(255, 255, 255);
        }
      }
    }
    @media screen and (max-width: 350px) {
      .header_username_hidden {
        margin-left: 15px;
      }
    }
  }
`;
