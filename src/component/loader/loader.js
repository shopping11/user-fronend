import React from "react";
import ReactLoading from "react-loading";
import { ContainerLoader } from "./loader.style";

function Loader() {
  return (
    <ContainerLoader>
      <ReactLoading type={"balls"} color={"blue"} height={"5%"} width={"5%"} />
    </ContainerLoader>
  );
}

export default Loader;
