import React, { useState } from "react";
import {
  BoxContainer,
  BodyBackGround,
  ComfirmScreen,
} from "./alertWarning.style";
import { IoAlertCircleOutline } from "react-icons/io5";

function AlertWarning(props) {
  const { alertDialog, setAlertDialog } = props;
  return (
    <>
      { alertDialog.isOpen ? (
        <BodyBackGround>
          <BoxContainer>
            <ComfirmScreen>
              <div className="content">
                <div>
                  <IoAlertCircleOutline size={80} />
                </div>
                <div style={{ fontWeight: "bold", marginTop: "10px" }}>
                  {alertDialog.title}
                </div>
                <div>
                  <button
                    onClick={() =>
                      setAlertDialog({ ...alertDialog, isOpen: false })
                    }
                  >
                    ตกลง
                  </button>
                </div>
              </div>
            </ComfirmScreen>
          </BoxContainer>
        </BodyBackGround>
      ) : null}
    </>
  );
}

export default AlertWarning;
