import React from 'react'
import { useSelector }  from 'react-redux'
import LoaderTempleate from './loader.template'
function LoadingController() {
    const auth = useSelector((state) => state.auth)
    const order = useSelector((state) => state.order)

    return (
        <>
            {auth.loading ? <LoaderTempleate /> : null}
            {order.loading ? <LoaderTempleate /> : null}
        </>
    )
}

export default LoadingController