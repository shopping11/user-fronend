import React from 'react'
import Header from '../header/header'
import CategoryMenu from '../categoryMenu/categoryMenu'
function Layout(props) {
    return (
        <div>
            <Header {...props}/>
            { props.categoryNenu ?    <CategoryMenu /> : null }
            { props.children }
        </div>
    )
}

export default Layout
