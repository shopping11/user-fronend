import { orderType } from "../action/order/orderType"

const initial = {
    loading: false,
    errors: [],
    orders: [],
    filters: []
}
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initial, action) => { 
        // eslint-disable-next-line default-case
    switch (action.type) {
        case orderType.CREATE_ORDER_REQUEST:
            state = {
                ...state,
                loading: true,
                errors: []
            }
            break
        case orderType.CREATE_ORDER_SUCCESS:
            state = {
                ...state,
                loading: false
            }
            break
        case orderType.CREATE_ORDER_FAILURE:
            state = {
                ...state,
                loading: false,
                errors: action.payload
            }
            break   
        case orderType.GET_ORDER_HOSTORY_REQUEST:
            state = {
                ...state,
                loading: true,
                errors: []
            }
            break
        case orderType.GET_ORDER_HOSTORY_SUCCESS:
            state = {
                ...state,
                loading: false,
                orders: action.payload.data,
                filters: action.payload.filter
            }
            break
        case orderType.GET_ORDER_HOSTORY_FAILURE:
            state = {
                ...state,
                loading: false,
                errors: action.payload
            }
            break
        case orderType.CANCLE_ORDER_REQUEST:
            state = {
                ...state,
                loading: true,
                errors: []
            }
            break
        case orderType.CANCLE_ORDER_SUCCESS:
            state = {
                ...state,
                loading: false,
            }
            break
        case orderType.CANCLE_ORDER_FAILURE:
            state = {
                ...state,
                loading: false,
                errors: action.payload
            }
            break   
    }
    
    return state
}