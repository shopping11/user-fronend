import { combineReducers } from "redux";
import productReducer from "./productReducer";
import authReducer from "./authReducer";
import categoryReducer from "./categoryReducer";
import cartReducer from "./cartReducer";
import orderReducer from "./orderReducer";

const rootReducer = combineReducers({
    product: productReducer,
    auth: authReducer,
    category: categoryReducer,
    cart: cartReducer,
    order: orderReducer
});
export default rootReducer;
