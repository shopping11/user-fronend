import { cartType } from "../action/cart/cartType"

const initial = {
    loading: false,
    subLoading: false,
    errors: [],
    cart: []
}
// eslint-disable-next-line import/no-anonymous-default-export
export default (state = initial, action) => { 
        // eslint-disable-next-line default-case
    switch (action.type) {
        case cartType.GET_CART_REQUEST:
            state = {
                ...state,
                loading: true,
                errors: []
            }
            break
        case cartType.GET_CART_SUCCESS:
                state = {
                    ...state,
                    loading: false,
                    cart: action.payload
                }
            break
        case cartType.GET_CART_FAILURE:
                state = {
                    ...state,
                    loading: false,
                    errors: action.payload
                }
            break
        case cartType.ADD_CART_REQUEST:
            state = {
                ...state,
                loading: true,
                errors: []
            }
            break
        case cartType.ADD_CART_SUCCESS:
            state = {
                ...state,
                loading: false,
                cart: action.payload
            }
            break
        case cartType.ADD_CART_FAILURE:
                state = {
                    ...state,
                    loading: false,
                    errors: action.payload
                }
            break
        case cartType.DELETE_CART_REQUEST:
            state = {
                ...state,
                subLoading: true,
                errors: []
            }
            break
        case cartType.DELETE_CART_SUCCESS:
            state = {
                ...state,
                subLoading: false,
                cart: action.payload
            }
            break     
        case cartType.DELETE_CART_FAILURE:
            state = {
                ...state,
                subLoading: false,
                errors: action.payload
            }
            break        
    }
    
    return state
}