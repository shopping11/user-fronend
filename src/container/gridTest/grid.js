// import React from "react";
// import {
//   Container,
//   Main,
//   NavBar,
//   ContentBox,
//   SideBar,
//   Content1,
//   Content2,
//   Content3,
//   Footer,
// } from "./grid.style";
// function Grid() {
//   return (
//     <Container>
//       <NavBar>NavBar</NavBar>
//       <Main>Main</Main>
//       <SideBar>SideBar</SideBar>
//       <ContentBox>
//         <Content1>Content1</Content1>
//         <Content2>Content2</Content2>
//         <Content3>Content3</Content3>
//       </ContentBox>
//       <Footer>Footer</Footer>
//     </Container>
//   );
// }

// export default Grid;

import React from "react";
import Layout from "../../component/layout/layout";
import { CategoryMenu } from "./grid.style";

function Grid() {
  return (
    <CategoryMenu>
      <div className="nav-area">
        <ul>
          <li>
            <a href="#">home</a>
          </li>
          <li>
            <a href="#">About</a>
          </li>
          <li>
            <a href="#">Services</a>
            <ul>
              <li>
                <a href="#">Web Design</a>
              </li>
              <li>
                <a href="#">Web Development</a>
              </li>
              <li>
                <a href="#">Email Marketing</a>
              </li>
              <li>
                <a href="#">Graphics Design</a>
                <ul>
                  <li>
                    <a href="#">Logo Design</a>
                  </li>
                  <li>
                    <a href="#">Card Design</a>
                  </li>
                  <li>
                    <a href="#">Poster Design</a>
                  </li>
                  <li>
                    <a href="#">Template Design</a>
                    <ul>
                      <li>
                        <a href="#">Web Template</a>
                      </li>
                      <li>
                        <a href="#">Email Template</a>
                      </li>
                      <li>
                        <a href="#">News Template</a>
                      </li>
                      <li>
                        <a href="#">Bio Template</a>
                      </li>
                      <li>
                        <a href="#">Wedding Template</a>
                      </li>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Brochure Design</a>
                  </li>
                </ul>
              </li>
              <li>
                <a href="#">SEO</a>
              </li>
            </ul>
          </li>
          <li>
            <a href="#">Portfolio</a>
          </li>
          <li>
            <a href="#">Contact</a>
          </li>
        </ul>
      </div>
    </CategoryMenu>
  );
}

export default Grid;
