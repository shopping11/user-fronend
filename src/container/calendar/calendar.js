import * as React from "react";
import TextField from "@mui/material/TextField";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import StaticDatePicker from "@mui/lab/StaticDatePicker";
import { format, addHours } from "date-fns";
import DateRangePicker from "@mui/lab/DateRangePicker";
import Box from "@mui/material/Box";

function disableRandomDates(date) {
  // console.log(format(new Date(date), 'dd/MM/yyyy') === format(new Date(), 'dd/MM/yyyy'))
  return (
    format(new Date(date), "dd/MM/yyyy") ===
    format(new Date("2022-01-19T13:42:58.389+00:00"), "dd/MM/yyyy")
  );
  // return format(date, 'dd/MM/yyyy - HH:mm')
}

export default function Calendar() {
  const [value, setValue] = React.useState([null, null]);

  const handleCheckIsSelectDisbleDate = (newValue) => {
    const firstDate = format(new Date(newValue[0]), "dd/MM/yyyy");
    const secondDAte = format(new Date(newValue[1]), "dd/MM/yyyy");
    const check = "19/01/2022";
    if (check >= firstDate && check <= secondDAte) {
      window.alert("error cant select this date");
      setValue([null, null]);
    } else {
        setValue(newValue);
        console.log(newValue)
    }
  };

  return (
    <>
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <DateRangePicker
          startText="Check-in"
          endText="Check-out"
          value={value}
          disablePast
          shouldDisableDate={disableRandomDates}
          onChange={(newValue) => {
            handleCheckIsSelectDisbleDate(newValue);
          }}
          renderInput={(startProps, endProps) => (
            <>
              <TextField {...startProps} />
              <Box sx={{ mx: 2 }}> to </Box>
              <TextField {...endProps} />
            </>
          )}
        />
      </LocalizationProvider>
      {/* <LocalizationProvider dateAdapter={AdapterDateFns}>
        <StaticDatePicker
          orientation="landscape"
          openTo="day"
          value={value}
          shouldDisableDate={disableRandomDates}
          onChange={(newValue) => {
            setValue(newValue);
          }}
          renderInput={(params) => <TextField {...params} />}
        />
      </LocalizationProvider> */}
    </>
  );
}
