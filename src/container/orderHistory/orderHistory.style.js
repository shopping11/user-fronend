import styled from "styled-components";
const font = "'Prompt', sans-serif";

export const Container = styled.div`
  margin-top: 50px;
  display: grid;
  grid-template-columns: 1fr;
  place-items: center;
  padding-bottom: 50px;
  overflow-x: hidden;

  .header_name {
    font-size: 35px;
    font-weight: bold;
    font-family: ${font};
  }

  .order_state {
    margin-top: 50px;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr;
    gap: 50px;
    padding: 10px 20px;
    border-bottom: 1px solid;
    @media screen and (max-width: 875px) {
      gap: 0px;
    }
  }
  .order_state_name {
    padding-left: 20px;
    padding-right: 20px;
    cursor: pointer;
  }
  .order_state_name_active {
    padding-left: 20px;
    padding-right: 20px;
    padding-bottom: 5px;
    height: 100%;
    border-bottom: 7px solid;
    cursor: pointer;
  }
  .order_list_head_pending{
    margin-top: 30px;
    width: 800px;
    display: grid;
    border-bottom: 1px solid;
    padding: 10px 0px;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    @media screen and (max-width: 875px) {
      width: 100%;
    }
  }
  .order_list_head {
    margin-top: 30px;
    width: 800px;
    display: grid;
    border-bottom: 1px solid;
    padding: 10px 0px;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    @media screen and (max-width: 875px) {
      width: 100%;
    }
  }
  .order_list_content {
    padding: 20px 0px;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr;
    border-bottom: 1px solid #ddd;
    width: 800px;
   
    @media screen and (max-width: 875px) {
      width: 100%;
    }
  }
  .order_list_content:nth-child(odd) {
    background: #dee2e6;
  }

  .order_list_content_pending {
    padding: 20px 0px;
    display: grid;
    grid-template-columns: 1fr 1fr 1fr 1fr 1fr;
    border-bottom: 1px solid #ddd;
    width: 800px;

    @media screen and (max-width: 875px) {
      width: 100%;
    }
  }

  .order_list_content_pending:nth-child(odd) {
    background: #dee2e6;
  }

  .order_date_item {
    display:flex ;
  }
  .pop_up_detail_button > span {
    cursor: pointer ;
  }
  .pop_up_detail_button {

    display:none ;
    padding-left: 5px;
    padding-right: 5px;
    
  }
  .hidden_detail {
    text-align: start;
    grid-column:1/6 ;
    border-top:1px solid  #ddd;
    display: none;
  }
  .hidden_detail > div:nth-child(1){
    padding:8px 40px ;
    font-size: 15px;
  }
  .hidden_detail > div:nth-child(2){
    padding:0px 40px ;
    font-size: 13px;
  }
  .action_button {
    display:flex ;
    margin-top: 10px;
    cursor: pointer;
  }
  .action_button > div{
    padding: 10px ;
  }
  .enablePreviosButton {
    display: none;
  }
  .order_product_item {
    text-align:start;
    /* overflow: hidden;
   text-overflow: ellipsis;
   display: -webkit-box;
   -webkit-line-clamp: 3; /* number of lines to show */
   /* -webkit-box-orient: vertical; */ */
  }
  .cancle_order_booking_action {
    cursor: pointer;
   
  }
  .cancle_order_booking_action:hover {
    color: red;
  }

  @media screen and (max-width: 500px) {
    .order_list_head {
      grid-template-columns: 1fr 1fr 1fr;
    }
    .order_list_content {
      grid-template-columns: 1fr 1fr 1fr;
    }
    .order_list_head_pending{
      grid-template-columns: 1fr 1fr 1fr 1fr;
    }
    .order_list_content_pending {
      grid-template-columns: 1fr 1fr 1fr 1fr;
    }
    .order_product_item {
      display:none ;
    }
    .order_list_head_pending > div:nth-child(4){
      display:none ;
    }
    .order_list_head  > div:nth-child(4){ 
      display:none ;
    }
    .pop_up_detail_button {
      display: block;
    }
    .cancle_order_booking_action {
    text-align: start;
  }
    .hidden_detail {
      display: block;
    }
  }
`;
