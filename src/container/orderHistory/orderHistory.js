import React, { useEffect, useState } from "react";
import Layout from "../../component/layout/layout";
import { GlobalStyle } from "../../component/ui/globalStyle/global.style";
import { Container } from "./orderHistory.style";
import { useDispatch, useSelector } from "react-redux";
import { GetOrderByUser, CancleOrder } from "../../action/order/orderAction";
import Loader from "../../component/loader/loader";
import DateIsotoString from "../../component/dateIsotoString/dateIsotoString";
import ConfirmAlert from "../../component/confirmAlert/confirmAlert";

function OrderHistory(props) {
  const search = props.location.search;
  const dispatch = useDispatch();
  const order = useSelector((state) => state.order);
  const [orderState, setOrderState] = useState("pending");
  const [popupDetial, setPopupDetail] = useState([]);
  const [confirmDialog, setConfirmDialog] = useState({
    isOpen: false,
    title: "",
  });

  const [page, setPage] = useState(1);

  useEffect(() => {
    handleHeaderActive();
  }, [props.location.search]);

  const handleHeaderActive = () => {
    const state = new URLSearchParams(search).get("state") || "pending";
    setOrderState(state);
    setPage(1);
    const payload = {
      page: 1,
    };
    dispatch(GetOrderByUser(state, payload));
  };

  const handleChangeState = (state) => {
    props.history.push({
      pathname: "/order_history",
      search: `state=${state}`,
    });
  };

  const handleNextPage = () => {
    if (order.filters.hasNextPage) {
      setPage(order.filters.nextPage);
      const state = new URLSearchParams(search).get("state");
      const payload = {
        page: order.filters.nextPage,
      };
      dispatch(GetOrderByUser(state, payload));
    }
  };

  const handlePreviousPage = () => {
    if (order.filters.hasPrevPage) {
      setPage(order.filters.prevPage);
      const state = new URLSearchParams(search).get("state");
      const payload = {
        page: order.filters.prevPage,
      };
      dispatch(GetOrderByUser(state, payload));
    }
  };

  const handleCancleOrder = (id) => {
    const payloadPage = {
      page: page,
    };
    const payload = {
      id: id,
    };
    dispatch(CancleOrder(payload, payloadPage));
  };

  return (
    <Layout>
      <GlobalStyle color={1} />
      <Container>
        <div className="header_name">ประวัติสั่งจอง</div>
        <div className="order_state">
          <div
            className={
              orderState === "pending"
                ? "order_state_name_active"
                : "order_state_name"
            }
            onClick={() => handleChangeState("pending")}
          >
            รอดำเนินการ
          </div>
          <div
            className={
              orderState === "success"
                ? "order_state_name_active"
                : "order_state_name"
            }
            onClick={() => handleChangeState("success")}
          >
            สำเร็จ
          </div>
          <div
            className={
              orderState === "cancle"
                ? "order_state_name_active"
                : "order_state_name"
            }
            onClick={() => handleChangeState("cancle")}
          >
            ยกเลิก
          </div>
        </div>
        {order.loading ? null : (
          <>
            <div
              className={
                orderState === "pending"
                  ? "order_list_head_pending"
                  : "order_list_head"
              }
            >
              <div>วันที่ทำรายการ</div>
              <div>หมายเลขคำสั่งจอง</div>
              <div>ยอดที่ชำระ</div>
              <div>รายการที่จอง</div>
            </div>
            {order.orders.map((order, indexOrder) => (
              <div
                className={
                  orderState === "pending"
                    ? "order_list_content_pending"
                    : "order_list_content"
                }
                key={order.id}
              >
                <div className="order_date_item">
                  <div className="pop_up_detail_button">
                    {popupDetial[indexOrder] ? (
                      <span
                        onClick={() => {
                          const newPopupDetial = [...popupDetial];
                          newPopupDetial[indexOrder] = false;
                          setPopupDetail(newPopupDetial);
                        }}
                        style={{ padding: "10px" }}
                      >
                        -
                      </span>
                    ) : (
                      <span
                        onClick={() => {
                          const newPopupDetial = [...popupDetial];
                          newPopupDetial[indexOrder] = true;
                          setPopupDetail(newPopupDetial);
                        }}
                        style={{ padding: "10px" }}
                      >
                        +
                      </span>
                    )}
                  </div>
                  <div>{DateIsotoString(order.createdAt)}</div>
                </div>
                <div>{order.code}</div>
                <div>{order.totalPrice}</div>
                <div className="order_product_item">
                  {order.orderItems.map((orderItem, index) => (
                    <div key={index}>
                      {orderItem.name}{" "}
                      {orderItem?.productPickType ? (
                        <>
                          {orderItem?.productPickType.mainType}
                          {orderItem?.productPickType?.subType ? (
                            <>,{orderItem?.productPickType?.subType}</>
                          ) : null}{" "}
                        </>
                      ) : null}
                    </div>
                  ))}
                </div>
                {orderState === "pending" ? (
                  <div
                    className="cancle_order_booking_action"
                    onClick={() =>
                      setConfirmDialog({
                        isOpen: true,
                        title: "คุณเเน่ใจว่าต้องการยกเลิกคำสั่งจอง",
                        onConfirm: () => handleCancleOrder(order.id),
                      })
                    }
                  >
                    ยกเลิกคำสั่งจอง
                  </div>
                ) : null}
                {popupDetial[indexOrder] ? (
                  <div className="hidden_detail">
                    <div>รายการที่จอง</div>
                    <div>
                      {order.orderItems.map((orderItem, index) => (
                        <div key={index}>
                          {orderItem.name}{" "}
                          {orderItem?.productPickType ? (
                            <>
                              {orderItem?.productPickType.mainType}
                              {orderItem?.productPickType?.subType ? (
                                <>,{orderItem?.productPickType?.subType}</>
                              ) : null}{" "}
                            </>
                          ) : null}
                        </div>
                      ))}
                    </div>
                  </div>
                ) : null}
              </div>
            ))}
            <div className="action_button">
              <div
                onClick={handlePreviousPage}
                className={
                  order.filters.hasPrevPage
                    ? "disablePreviosButton"
                    : "enablePreviosButton"
                }
              >
                Previous
              </div>
              <div
                onClick={handleNextPage}
                className={
                  order.filters.hasNextPage
                    ? "disablePreviosButton"
                    : "enablePreviosButton"
                }
              >
                Next
              </div>
            </div>
          </>
        )}
      </Container>
      {order.loading ? <Loader /> : null}
      <ConfirmAlert
        confirmDialog={confirmDialog}
        setConfirmDialog={setConfirmDialog}
      />
    </Layout>
  );
}

export default OrderHistory;
