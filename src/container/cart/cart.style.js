// import styled from "styled-components";

// const font = "'Prompt', sans-serif";

// export const Container = styled.div`
//   /* background:red ; */
//   margin: 50px 350px;
//   .header_name {
//     font-size: 35px;
//     font-weight: bold;
//     font-family: ${font};
//   }
//   .loader {
//     place-items: center;
//     display: grid;
//   }
// `;

import styled from "styled-components";

const font = "'Prompt', sans-serif";

export const Container = styled.div`
  margin-top: 50px;
  display: grid;
  grid-template-columns: 1fr;
  place-items: center;
  overflow-x: hidden;
  padding-bottom: 50px;

  @media screen and (max-width: 750px) {
    margin: 20px 0px;
  }
  .header_name {
    padding: 20px;
    width: 900px;
    font-size: 35px;
    border-bottom: 1px groove;
    font-weight: bold;
    font-family: ${font};
    @media screen and (max-width: 900px) {
      width: 100%;
      padding-right: 0px;
      padding-left: 0px;
    }
  }
  .cart_content {
    padding: 20px;
    width: 900px;
    display: grid;
    grid-template-columns: 100px 1fr;
    border-bottom: 1px groove;
    img {
      width: 100px;
    }
    @media screen and (max-width: 900px) {
      width: 100%;
      padding-right: 0px;
      padding-left: 0px;
    }
    .cart_item_detail {
      display: grid;
      grid-template-columns: repeat(5, 1fr);
      /* place-items: center; */
    }
    .item_product_name_price {
      display: grid;
      place-items: center;
    }
    .item_product_name_price > .name_item {
      font-size: 20px;
      font-weight: bold;
      padding-bottom: 7px;
      font-family: ${font};
    }
    .cart_item_type {
      font-family: ${font};
      display: grid;
      place-items: center;
    }
    .cart_item_qty {
      display: grid;
      place-items: center;
    }
    .cart_item_date {
      display: grid;
      place-items: center start;
    }
    .action {
      display: grid;
      place-items: center;
    }
    @media screen and (max-width: 500px) {
      .item_product_name_price {
        text-align: start;
      }
      .cart_item_detail {
        grid-template-columns: 1fr;
        padding-left: 50px;
      }
      .item_product_name_price{
        place-items: start;
      }
      .cart_item_type {
        place-items: start;
      }
      .cart_item_qty {
        place-items: start;
      }
      .cart_item_date {
        place-items: start;
      }
      .action {
        place-items: start;
      }
    }
  }
  .total_price {
    margin-top: 30px;
  }
  .button_place_order {
    margin-top: 20px;
    background-color: #4caf50; /* Green */
    border: none;
    color: white;
    padding: 20px 70px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    border-radius: 50px;
    cursor: pointer;
  }
  .cart_item_date {
    text-align: start;
  }
  .delete_item_cart {
    cursor: pointer;
  }
  .delete_item_cart:hover {
    color: red;
  }
  .cart_no_item {
    color: red;
    margin-top: 40px;
  }
`;
