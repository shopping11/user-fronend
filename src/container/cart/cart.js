// import React from "react";
// import Layout from "../../component/layout/layout";
// import { GlobalStyle } from "../../component/ui/globalStyle/global.style";
// import { useDispatch, useSelector } from "react-redux";
// import { Container } from "./cart.style";
// import ReactLoading from "react-loading";

// function Cart() {
//   const cart = useSelector((state) => state.cart);

//   return (
//     <Layout>
//       <GlobalStyle color={1} />
//       <Container>
//         <div className="header_name">ตะกร้า</div>
//         <hr />
//         {cart.loading ? (
//           <div className="loader">
//             <ReactLoading
//               type={"balls"}
//               color={"blue"}
//               height={"5%"}
//               width={"5%"}
//             />
//           </div>
//         ) : (
//           <div></div>
//         )}
//       </Container>
//     </Layout>
//   );
// }

// export default Cart;

import React, { useEffect, useState } from "react";
import Layout from "../../component/layout/layout";
import { GlobalStyle } from "../../component/ui/globalStyle/global.style";
import { useDispatch, useSelector } from "react-redux";
import { Container } from "./cart.style";
import ReactLoading from "react-loading";
import { DeleteItemInCart } from "../../action/cart/cartAction";
import { format } from "date-fns";
import { CreateOrder } from "../../action/order/orderAction";
import ConfirmAlert from "../../component/confirmAlert/confirmAlert";
import LoadingController from "../../component/loadingDialog/loadingController";
import { useHistory } from "react-router-dom";

function Cart() {
  const cart = useSelector((state) => state.cart);
  const dispatch = useDispatch();
  const [confirmDialog, setConfirmDialog] = useState({
    isOpen: false,
    title: "",
  });
  let history = useHistory();

  const handleDeleteItemInCart = (cart) => {
    let payload = {
      id: cart.id,
    };
    if (cart.productPickType?.mainType) {
      payload.mainType = cart.productPickType?.mainType;
    }
    if (cart.productPickType?.subType) {
      payload.subType = cart.productPickType?.subType;
    }

    dispatch(DeleteItemInCart(payload));
  };

  const handlePlaceOrder = () => {
    const payload = {
      orderItems: cart.cart.cartItems,
    };

    dispatch(CreateOrder(payload)).then((err) => {
      if (!err) {
        history.push('/orderSuccess')
      }
    });
  };
  return (
    <Layout>
      <ConfirmAlert
        confirmDialog={confirmDialog}
        setConfirmDialog={setConfirmDialog}
      />
      <LoadingController />
      <GlobalStyle color={1} />
      <Container>
        <div className="header_name">ตะกร้า</div>
        {cart.loading ? (
          <>
            <ReactLoading
              type={"balls"}
              color={"blue"}
              height={"5%"}
              width={"5%"}
            />
          </>
        ) : (
          <>
            {cart.cart?.cartItems?.length > 0 ? (
              <>
                {cart.cart.cartItems.map((cartItem, index) => (
                  <div className="cart_content" key={index}>
                    <div>
                      <img
                        src={cartItem.picture.src || ""}
                        onError={(ev) => (ev.target.src = "/img/no_image.jpg")}
                      />
                    </div>
                    <div className="cart_item_detail">
                      <div className="item_product_name_price">
                        <div className="name_item">{cartItem.name}</div>
                        <div className="item_product_price">
                          &#3647;{cartItem.price}
                        </div>
                      </div>
                      <div className="cart_item_type">
                        {cartItem?.productPickType?.mainType}
                        {cartItem?.productPickType?.subType ? (
                          <>,{cartItem?.productPickType?.subType}</>
                        ) : null}
                      </div>
                      <div className="cart_item_qty">{cartItem.quantity}</div>
                      <div className="cart_item_date">
                        {format(
                          new Date(cartItem.bookingDates[0]),
                          "dd/MM/yyyy"
                        )}
                        {cartItem.bookingDates.length > 1 ? (
                          <>
                            -
                            {format(
                              new Date(
                                cartItem.bookingDates[
                                  cartItem.bookingDates.length - 1
                                ]
                              ),
                              "dd/MM/yyyy"
                            )}
                          </>
                        ) : null}
                      </div>
                      <div className="action">
                        <div
                          onClick={() => handleDeleteItemInCart(cartItem)}
                          className="delete_item_cart"
                        >
                          ลบ
                        </div>
                      </div>
                    </div>
                  </div>
                ))}
                <div className="total_price">
                  ยอดชำระ &#3647;{cart.cart?.totalPrice}
                </div>
                <button
                  className="button_place_order"
                  onClick={() =>
                    setConfirmDialog({
                      isOpen: true,
                      title:
                        "คุณต้องการจองหรือไม่ !จ่ายเงินหน้าร้านรับของที่ร้าน",
                      onConfirm: () => {
                        handlePlaceOrder();
                      },
                    })
                  }
                >
                  สั่งจอง
                </button>
              </>
            ) : (
              <div className="cart_no_item">
                ไม่พบข้อมูลในตะกร้าสินค้า{console.log(cart.loading)}
              </div>
            )}
          </>
        )}
      </Container>
    </Layout>
  );
}

export default Cart;
