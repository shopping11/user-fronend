import styled from "styled-components";

export const Container = styled.div`
  margin: 20px 150px;
  display: grid;
  overflow-x: hidden;
  /* background: red; */
  place-items:center;

  @media screen and (max-width: 730px) {
    margin: 20px 0px;
  }
`;

export const ProductCards = styled.div`
  margin: 10px 2px;
  display: grid;
  grid-template-columns: repeat(5, minmax(100px, 250px));
  gap: 40px ;

  @media screen and (max-width: 1500px) {
    grid-template-columns: repeat(4, minmax(100px, 350px));
  }
  @media screen and (max-width: 1300px) {
    grid-template-columns: repeat(3, minmax(100px, 350px));
  }
  @media screen and (max-width: 970px) {
    grid-template-columns: repeat(2, minmax(100px, 350px));
  }
  @media screen and (max-width: 730px) {
    margin: 10px auto;
    grid-template-columns: repeat(1, minmax(100px, 350px));
  }

  .card {
    display: grid;
    box-shadow: 0px 2px 2px #aaaaaa;
    grid-template-columns: 1fr;
    grid-template-rows: 250px 100px 50px;
    cursor: pointer;
  }
  .card:hover {
    transform: translateY(-5px);
    box-shadow: 0 0 2px 2px rgb(15,118,227);
  }
  img {
    float: left;
    height: 100%;
    width: 100%;
    object-fit: cover;
  }

  .card_img {
    background: red;
  }

  .card_content {
    line-height: 1.5;
    font-size: 0.9em;
    padding: 15px;
    background: #fafafa;

    /* p{
      margin: 0;
    } */
  }
  .card_info {
    display: grid;
    place-items: center;
  }

  .product_name {
    margin: 0;
    /* height: px;
    overflow: hidden; */
    display: -webkit-box;
    -webkit-line-clamp: 1;
    -webkit-box-orient: vertical;
    overflow: hidden;
    text-overflow: ellipsis;
  }

  .product_price{ 
    color: rgb(15,118,227);
    font-weight:bold;
  }
`;

export const PaginateBottom = styled.div`
  display: grid;
  grid-template-columns: 1fr;
  margin-top: 20px;
  justify-items: center;
`;
