import React from "react";
import { Container } from "./orderSuccess.style";
// import { useHistory } from 'react-router-dom';
function OrderSuccess() {
    // let history = useHistory();
    const handleRouteChange = () => { 
       window.location.href = '/'
    }
  return (
    <Container>
      <div className="icon-success">
        <img src="/img/success.png" />
      </div>
      <div style={{ marginTop: "30px", fontSize: "20px", fontWeight: "bold" }}>
        จองสำเร็จ มารับที่ร้านจ่ายตังหน้าร้านได้เลย
      </div>
      <div className="action" style={{ marginTop: "30px" }}>
              <button onClick={handleRouteChange}>กลับหน้าหลัก</button>
      </div>
    </Container>
  );
}

export default OrderSuccess;
