import styled from "styled-components";

export const Container = styled.div`
  margin-top: 80px;
  .icon-success > img {
    width: 70px;
  }
  .action button {
    background-color: #4caf50; /* Green */
    border: none;
    color: white;
    padding: 15px 150px;
    text-align: center;
    text-decoration: none;
    display: inline-block;
    font-size: 16px;
    border-radius: 50px;
    cursor: pointer;
  }
`;
