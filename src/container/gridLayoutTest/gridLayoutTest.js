// import React, { useState, useEffect } from "react";
// import Layout from "../../component/layout/layout";
// import { Navigator, Container } from "./gridLayoutTest.style";

// const DataProductType = [
//   {
//     nameType: "Color",
//     productType: [
//       { type: "เเดงลายลูกไม้" },
//       { type: "ส้มลายดอก" },
//       { type: "เขียวลายทาง" },
//     ],
//   },
//   {
//     nameType: "Size",
//     productType: [{ type: "M" }, { type: "L" }, { type: "S" }, { type: "x" }],
//   },
// ];

// const Thumbnail = ({ imgs, index, setIndex }) => {
//   return (
//     <div className="tumbnail">
//       {imgs.map((imgsrc, i) => (
//         <img
//           src={imgsrc}
//           onClick={() => setIndex(i)}
//           className={index === i ? "active" : ""}
//           key={i}
//         />
//       ))}
//     </div>
//   );
// };

// const Slideshow = ({ imgs }) => {
//   const [index, setIndex] = useState(0);

//   return (
//     <div className="content-1">
//       <div className="content-1-1">
//         <img className="mainImg" src={imgs[index]} />
//         <div className="actions"></div>
//       </div>
//       <div className="content-1-2">
//         <Thumbnail imgs={imgs} index={index} setIndex={setIndex} />
//       </div>
//     </div>
//   );
// };

// const ProductTypePicker = ({ data, setProductPickType, productPickType }) => {
//   useEffect(() => {
//     const initialProductPickType = data.map((index) => ({
//       nameType: index.nameType,
//       productType: "",
//     }));

//     setProductPickType(initialProductPickType);
//   }, []);

//   const handlePickType = (type, index) => {
//     const newProductPickType = [...productPickType];
//     if (newProductPickType[index].productType === type.type) {
//       newProductPickType[index].productType = "";
//     } else {
//       newProductPickType[index].productType = type.type;
//     }

//     setProductPickType(newProductPickType);
//   };

//   return (
//     <>
//       {data.map((index, i) => (
//         <div className="typePickProduct" key={i}>
//           <div className="nameProduct">{index.nameType}</div>
//           <div className="typeProduct">
//             {index.productType.map((type, y) => (
//               <button
//                 key={y}
//                 onClick={() => handlePickType(type, i)}
//                 className={
//                   productPickType?.[i].productType === type.type ? "active" : ""
//                 }
//               >
//                 {type.type}
//               </button>
//             ))}
//           </div>
//         </div>
//       ))}
//     </>
//   );
// };

// function GridLayoutTest() {
//   const [productPickType, setProductPickType] = useState();
//   const [errorValidate, setErrorValidate] = useState({});

//   let error = [];
//   let formIsValid = true;

//   const HandleValidation = () => {
//     productPickType.map((index) => {
//       if (!index?.productType) {
//         error = !error?.productType
//           ? { ...error, productType: "กรุณาเลือกตัวเลือกของสินค้าก่อน" }
//           : error;
//         formIsValid = false;
//       }
//     });
//     setErrorValidate(error);
//   };

//   const handleSaveProduct = () => {
//     HandleValidation();
//     if (formIsValid) {
//       console.log(productPickType);
//     }
//   };
//   return (
//     <Layout>
//       {/* <Navigator>
//         <div>1</div>
//         <div>2</div>
//         <div>3</div>
//       </Navigator> */}
//       <Container>
//         <div className="conent1">
//           <div>
//             <Slideshow
//               imgs={[
//                 "https://pbs.twimg.com/media/DHyARNNXUAYXpfG.jpg",
//                 "https://cdn-local.mebmarket.com/meb/server1/150970/Thumbnail/book_detail_large.gif?2",
//                 "https://cdn-local.mebmarket.com/meb/server1/136510/Thumbnail/book_detail_large.gif?2",
//                 "https://cdn-local.mebmarket.com/meb/server1/136510/Thumbnail/book_detail_large.gif?2",
//               ]}
//             />
//           </div>
//           <div>
//             masdlaslmd dasldmaslk mdasmdl asmdlmasldm asldmlasmd lasmd;
//             mas;dm;as md,asm
//           </div>
//         </div>
//         <div className="content-2">
//           <h2 className="headerNameProduct">
//             ขอต้อนรับสู่ห้องเรียนนิยม (เฉพาะ) ยอดคน เล่ม 11.5 (ฉบับนิยาย)
//           </h2>
//           <div className="categoryProduct">ประเภท: manga</div>
//           <div className="priceProduct">ราคา: 3000</div>
//           <ProductTypePicker
//             setProductPickType={setProductPickType}
//             productPickType={productPickType}
//             data={DataProductType}
//           />
//           <div className="hireButton">
//             <div style={{ color: "red" }}>
//               {errorValidate?.productType ? errorValidate.productType : null}
//             </div>
//             <button onClick={handleSaveProduct}>เช่า</button>
//           </div>
//         </div>
//       </Container>
//     </Layout>
//   );
// }

// export default GridLayoutTest;

import React, { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";


function GridLayoutTest(props) {
  let history = useHistory();
  useEffect(() => { 

    console.log(1)

  },[])
  const handleClick = (event) => { 
    history.push({
      pathname: "/test/layout",
      search: `limit=10&page=${1}&orderBy=asc&search=`,
    });
  }
  return (
    <div>
      <button onClick={handleClick}></button>
    </div>
  );
}

export default GridLayoutTest;
