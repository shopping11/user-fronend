// import styled from "styled-components";

// export const Navigator = styled.div`
//   display: grid;
//   grid-template-columns: 1fr 1fr 1fr;
//   background: red;
// `;
// export const Container = styled.div`
//   display: grid;
//   grid-template-columns: 450px 500px;
//   grid-auto-rows: minmax(600px, auto);
//   justify-content: center;
//   background: white;

//   .content-1-1 {
//     img {
//       width: 50%;
//       object-fit: cover;
//     }
//   }

//   .content-1-2 {
//     margin-top: 50px;
//     .tumbnail {
//       margin-left: 10px;
//       cursor: pointer;
//     }

//     .tumbnail > img {
//       height: 100px;
//       width: 90px;
//     }
//     .active {
//       border: 2px solid #1db9b9;
//       transform: scale(1.1);
//       /* transition: all 300ms; */
//     }
//   }

//   @media screen and (max-width: 900px) {
//     grid-template-columns: 1fr;
//   }

//   .content-2 {
//     text-align: left;
//     margin-left:10px ;
//     .headerNameProduct {
//     }

//     .categoryProduct {
//       margin-top: 60px;
//     }

//     .priceProduct {
//       margin-top: 20px;
//     }

//     .typePickProduct {
//       display: flex;
//       margin-top: 40px;
//       /* display: inline-flex; */
//     }
//     .nameProduct {
//       margin-top: 8px;
//       width: 80px;
//       text-align: start;
//     }
//     .typeProduct {
//       margin-left: 40px;
//       text-align: start;
//       .active {
//       border: 2px solid #1db9b9;
//       transform: scale(1.1);
//     }
//     }

//     .typeProduct button {
//       overflow: visible;
//       cursor: pointer;
//       min-width: 5rem;
//       min-height: 2.125rem;
//       -moz-box-sizing: border-box;
//       box-sizing: border-box;
//       padding: 0.25rem 0.75rem;
//       margin: 0 8px 8px 0;
//       color: rgba(0, 0, 0, 0.8);
//       text-align: left;
//       border-radius: 2px;
//       border: 1px solid rgba(0, 0, 0, 0.09);
//       position: relative;
//       background: #fff;
//       outline: 0;
//       word-break: break-word;
//       display: inline-flex;
//       align-items: center;
//       justify-content: center;
//     }

//     .hireButton {
//       margin-top: 50px ;
//     }

//     .hireButton > button {
//       background-color: #4caf50; /* Green */
//       border: none;
//       color: white;
//       padding: 20px 100px;
//       text-align: center;
//       text-decoration: none;
//       display: inline-block;
//       font-size: 16px;
//       border-radius: 50px;
//       cursor: pointer;
//     }

//     @media screen and (max-width: 860px) {
//       text-align: center;

//       .typePickProduct {
//         justify-content: center;
//      }

//       .typeProduct {
//       margin-left: 10px;
//         }
//   }
// `;

import styled from "styled-components";

export const Container = styled.body`
  text-align: left;
  margin-left: 10px;
  .test {
    display: flex;
  }
  .p1 {
    margin-top: 8px;
    width: 80px;
    text-align: start;
    background: red;
  }
  .p2 {
    margin-left: 40px;
      text-align: start;
      background: green;
      width: 100px;
      display: grid;
      place-items: center;
`;
