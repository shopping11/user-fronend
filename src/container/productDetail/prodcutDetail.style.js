// import styled from "styled-components";

// const font = "'Prompt', sans-serif";

// export const Container = styled.div`
//   margin: 60px 0;
//   display: grid;
//   justify-content: center;
//   align-items: center;
//   overflow-x:hidden;

//   @media screen and (max-width: 860px) {
//     margin: 0px 0px;
//     justify-content: stretch;
//     align-items: normal;
//   }
// `;

// export const ProductDetailContent = styled.div`
//   display: grid;
//   grid-template-columns: 450px 500px;
//   grid-auto-rows: minmax(600px, auto);
//   justify-content: center;
//   background: white;
//   font-family: ${font};

//   @media screen and (max-width: 860px) {
//     grid-template-columns: 1fr;
//   }

//   .content-1 {
//     margin-top: 50px;
//   }

//   .content-1-1 {
//     img {
//       width: 250px;
//       height: 300px;
//       object-fit: cover;
//     }
//   }

//   .content-1-2 {
//     margin-top: 50px;
//     .tumbnail {
//       margin-left: 10px;
//       cursor: pointer;
//     }

//     .tumbnail > img {
//       height: 100px;
//       width: 90px;
//     }
//     .active {
//       border: 2px solid #1db9b9;
//       transform: scale(1.1);
//       /* transition: all 300ms; */
//     }
//   }

//   .content-2 {
//     text-align: left;
//     margin-left:10px ;
//     .headerNameProduct {
//     }

//     .categoryProduct {
//       margin-top: 60px;
//     }

//     .priceProduct {
//       margin-top: 20px;
//     }

//     .typePickProduct {
//       display: flex;
//       margin-top: 40px;
//       /* display: inline-flex; */
//     }
//     .nameProduct {
//       margin-top: 8px;
//       min-width: 80px;
//       text-align: start;
//     }
//     .typeProduct {
//       margin-left: 40px;
//       text-align: start;
//       .active {
//       border: 2px solid #1db9b9;
//       transform: scale(1.1);
//     }
//     }

//     .typeProduct button {
//       overflow: visible;
//       cursor: pointer;
//       min-width: 5rem;
//       min-height: 2.125rem;
//       -moz-box-sizing: border-box;
//       box-sizing: border-box;
//       padding: 0.25rem 0.75rem;
//       margin: 0 8px 8px 0;
//       color: rgba(0, 0, 0, 0.8);
//       text-align: left;
//       border-radius: 2px;
//       border: 1px solid rgba(0, 0, 0, 0.09);
//       position: relative;
//       background: #fff;
//       outline: 0;
//       word-break: break-word;
//       display: inline-flex;
//       align-items: center;
//       justify-content: center;
//     }

//     .hireButton {
//       margin-top: 50px ;
//     }

//     .hireButton > button {
//       background-color: #4caf50; /* Green */
//       border: none;
//       color: white;
//       padding: 20px 100px;
//       text-align: center;
//       text-decoration: none;
//       display: inline-block;
//       font-size: 16px;
//       border-radius: 50px;
//       cursor: pointer;
//     }

//     @media screen and (max-width: 860px) {
//       text-align: center;

//       .typePickProduct {
//         justify-content: center;
//      }

//       .typeProduct {
//       margin-left: 10px;
//         }
//   }
// `;

import styled from "styled-components";

const font = "'Prompt', sans-serif";

export const Container = styled.div`
  margin: 60px 0;
  display: grid;
  justify-content: center;
  align-items: center;
  overflow-x: hidden;

  @media screen and (max-width: 860px) {
    margin: 0px 0px;
    justify-content: stretch;
    align-items: normal;
  }
`;

export const ProductDetailContent = styled.div`
  display: grid;
  grid-template-columns: 450px 500px;
  grid-auto-rows: minmax(600px, auto);
  justify-content: center;
  background: white;
  font-family: ${font};

  @media screen and (max-width: 860px) {
    grid-template-columns: 1fr;
  }

  .content-1 {
    margin-top: 50px;
  }

  .content-1-1 {
    img {
      width: 250px;
      height: 300px;
      object-fit: cover;
    }
  }

  .content-1-2 {
    margin-top: 50px;
    .tumbnail {
      margin-left: 10px;
      cursor: pointer;
    }

    .tumbnail > img {
      height: 100px;
      width: 90px;
    }
    .active {
      border: 2px solid #1db9b9;
      transform: scale(1.1);
      /* transition: all 300ms; */
    }
  }

  .content-2 {
    text-align: left;
    margin-left:10px ;
    .headerNameProduct {
    }

    .categoryProduct {
      margin-top: 60px;
    }

    .priceProduct {
      margin-top: 20px;
    }

    .quantityItem{
      display: flex ;
      margin-top: 40px;
      place-items: center;

    }

    .addQuantity{
      padding: 0px;
      margin:0px ;
    }
    
    .addQuantity button{
      width: 45px ;
      height: 30px;
      background:white ;
      border: 1px solid;
      cursor: pointer ;
    }

    .addQuantity input{
      width:50px ;
      height: 26px;
      margin:0px ;
      padding:0px ;
  
    }

    input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
    }

    .inputQnatity{
      text-align:center;
    }

    .quantityProduct{
      margin-left: 10px;
      font-size: 15px;
    }

    .typePickProduct {
      display: flex;
      margin-top: 40px;
    }
    .nameProduct {
      margin-top: 8px;
      min-width: 80px;
      text-align: start;
    }
    .typeProduct {
      margin-left: 40px;
      text-align: start;
      .active {
      border: 2px solid #1db9b9;
      transform: scale(1.1);
    }
    .disable{
      opacity: 0.5;
      cursor: not-allowed;
    }
    }

    .typeProduct button {
      overflow: visible;
      cursor: pointer;
      min-width: 5rem;
      min-height: 2.125rem;
      -moz-box-sizing: border-box;
      box-sizing: border-box;
      padding: 0.25rem 0.75rem;
      margin: 0 8px 8px 0;
      color: rgba(0, 0, 0, 0.8);
      text-align: left;
      border-radius: 2px;
      border: 1px solid rgba(0, 0, 0, 0.09);
      position: relative;
      background: #fff;
      outline: 0;
      word-break: break-word;
      display: inline-flex;
      align-items: center;
      justify-content: center;
    }

    .hireButton {
      margin-top: 50px ;
    }

    .hireButton > button {
      background-color: #4caf50; /* Green */
      border: none;
      color: white;
      padding: 20px 70px;
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 16px;
      border-radius: 50px;
      cursor: pointer;
    }

    .hireButton > .button2 {
      margin-left: 10px;
    }

    @media screen and (max-width: 860px) {
      text-align: center;
     .typePickProduct {
        justify-content: center;
     }
     .quantityItem{
       justify-content: center;
      }
     .typeProduct {
        margin-left: 10px;
      }
      .button2 {
        margin-top: 12px;
      }
  }
`;
