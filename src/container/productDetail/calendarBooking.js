import React, { useState, useEffect } from "react";
import AdapterDateFns from "@mui/lab/AdapterDateFns";
import LocalizationProvider from "@mui/lab/LocalizationProvider";
import MobileDateRangePicker from "@mui/lab/MobileDateRangePicker";
import { format, addDays } from "date-fns";
import AlertWarning from "../../component/alertWarning/alertWarning";
import DateRangePickerDay from "@mui/lab/DateRangePickerDay";
import Badge from "@mui/material/Badge";
import { makeStyles } from "@material-ui/core/styles";
import { padding } from "@mui/system";

const useStyles = makeStyles({
  dayWithDotContainer: {
    position: "relative",
    padding: "4px 2px ",
  },
  dayWithDot: {
    position: "absolute",
    height: 0,
    width: 0,
    right: "67%",
    top: "70%",
    fontSize: "13px",
  },
});

function CalendarBooking({
  setProductPickType,
  productPickType,
  data,
  openCalendar,
  setOpenCalendar,
  defalultQuantity,
  pickQuantity,
}) {
  const [value, setValue] = useState([null, null]);
  const [disableDateList, setDisableDateList] = useState([]);
  const [bookingDate, setBookingDate] = useState([]);
  const [alertDialog, setAlertDialog] = useState({
    isOpen: false,
    title: "",
  });

  const classes = useStyles();

  useEffect(() => {
    if (data?.length > 0) {
      const findDisableDate = data.filter(
        (index) =>
          index.productPickType?.mainType +
            "-" +
            index.productPickType?.subType ===
          productPickType?.mainType + "-" + productPickType?.subType
      );
      setDisableDateList(findDisableDate);
    }
  }, [productPickType]);

  const getDates = (startDate, stopDate) => {
    let dateArray = [];
    let currentDate = new Date(startDate);
    let endDate = new Date(stopDate);

    while (currentDate <= endDate) {
      dateArray.push(format(currentDate, "yyyy/MM/dd"));
      currentDate = addDays(currentDate, 1);
    }
    return dateArray;
  };

  const handleCheckIsSelectDisbleDate = (newValue) => {
    if (newValue[0] && newValue[1]) {
      const dates = getDates(newValue[0], newValue[1]);
      for (const date of dates) {
        const findDate = disableDateList.find(
          (disableDate) =>
            format(new Date(disableDate.date), "yyyy/MM/dd") === date
        );
        if (findDate?.disbleStatus || findDate?.avaliableBook < pickQuantity) {
          window.alert("error cant select this date");
          setValue([null, null]);
          setBookingDate([]);
          return;
        }
      }
      setBookingDate(dates);
      setValue(newValue);
      return;
    }
    const date = format(new Date(newValue[0] || newValue[1]), "yyyy/MM/dd");
    setValue(newValue);
    setBookingDate([date]);
    return;
  };

  function disableDate(date) {
    if (disableDateList.length === 0) {
      return;
    }
    const findDateIndex = disableDateList.findIndex(
      (disableDate) =>
        format(new Date(disableDate.date), "yyyy/MM/dd") ===
        format(new Date(date), "yyyy/MM/dd")
    );
    if (
      disableDateList[findDateIndex]?.disbleStatus ||
      disableDateList[findDateIndex]?.avaliableBook < pickQuantity
    ) {
      // if (disableDateList[findDateIndex]?.disbleStatus) {
      return true;
    }

    return false;
  }

  const handleOnaccept = () => {
    if (!value[0] && !value[1]) {
      setAlertDialog({
        isOpen: true,
        title: "เลือกอย่างน้อย 1 วัน",
      });
      setOpenCalendar({ ...openCalendar, status: true });

      return;
    }
    openCalendar.nextFunction(bookingDate);
  };

  const handleCloseCalendar = () => {
    setOpenCalendar({ ...openCalendar, status: false });
    setValue([null, null]);
  };

  const handleDisable = (DateRangeDayProps) => {
    // if (DateRangeDayProps.disabled || DateRangeDayProps.outsideCurrentMonth) {
    //   return;
    // }
    if (DateRangeDayProps.day < new Date().setHours(0, 0, 0, 0)) {
      return;
    }
    if (DateRangeDayProps.outsideCurrentMonth) {
      return;
    }

    if (disableDateList.length === 0) {
      return productPickType.quantity;
    }

    const findDate = disableDateList.find(
      (disableDate) =>
        format(new Date(disableDate.date), "yyyy/MM/dd") ===
        format(new Date(DateRangeDayProps.day), "yyyy/MM/dd")
    );

    if (findDate) {
      return findDate.avaliableBook;
    }

    return productPickType.quantity || defalultQuantity;
  };

  const handleRenderDay = (date, DateRangeDayProps) => {
    return (
      <div className={classes.dayWithDotContainer} key={date.toString()}>
        <div>
          <DateRangePickerDay {...DateRangeDayProps} />
        </div>
        <div className={classes.dayWithDot}>
          {handleDisable(DateRangeDayProps)}
        </div>
      </div>
    );
  };

  return (
    <div className="calendar">
      <AlertWarning alertDialog={alertDialog} setAlertDialog={setAlertDialog} />
      <LocalizationProvider dateAdapter={AdapterDateFns}>
        <MobileDateRangePicker
          showToolbar={false}
          shouldDisableDate={disableDate}
          open={openCalendar.status}
          onAccept={handleOnaccept}
          onClose={handleCloseCalendar}
          disableCloseOnSelect={true}
          value={value}
          disablePast
          autoOk={true}
          onChange={(newValue) => {
            handleCheckIsSelectDisbleDate(newValue);
          }}
          renderInput={() => null}
          renderDay={handleRenderDay}
        />
      </LocalizationProvider>
    </div>
  );
}

export default CalendarBooking;
