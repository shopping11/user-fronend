import React, { useState, useEffect } from "react";
import Layout from "../../component/layout/layout";
import { GlobalStyle } from "../../component/ui/globalStyle/global.style";
import {
  Container,
  ProductDetailContent,
  // SlideshowImage,
} from "./prodcutDetail.style";
import { GetProductByName } from "../../action/product/productAction";
import { useDispatch, useSelector } from "react-redux";
import Loader from "../../component/loader/loader";
import ProductTypePicker from "./productTypePicker";
import CalendarBooking from "./calendarBooking";
import { AddItemToCart } from "../../action/cart/cartAction";
import { CreateOrder } from "../../action/order/orderAction";
import LoadingController from "../../component/loadingDialog/loadingController";
import { useHistory } from "react-router-dom";
import AlertWarning from "../../component/alertWarning/alertWarning";
import { APP_BAD_REQUEST } from "../../service-share/const/bad-request.const";
import { isLoggedBeforeAction } from "../../action/auth/authAction";

const Thumbnail = ({ imgs, showImg, setShowImg }) => {
  return (
    <div className="tumbnail">
      {imgs.map((imgsrc, i) => (
        <img
          src={imgsrc}
          onClick={() => setShowImg(imgsrc)}
          onMouseOver={() => setShowImg(imgsrc)}
          className={showImg === imgsrc ? "active" : ""}
          key={i}
        />
      ))}
    </div>
  );
};

const Slideshow = ({ imgs, showImg, setShowImg }) => {
  // const [index, setIndex] = useState(imgs[0]);
  useEffect(() => {
    setShowImg(imgs[0]);
  }, []);

  return (
    <div className="content-1">
      <div className="content-1-1">
        <img
          className="mainImg"
          src={showImg || ""}
          onError={(ev) => (ev.target.src = "/img/no_image.jpg")}
        />
        <div className="actions"></div>
      </div>
      <div className="content-1-2">
        <Thumbnail imgs={imgs} showImg={showImg} setShowImg={setShowImg} />
      </div>
    </div>
  );
};

function ProductDetail(props) {
  const [productPickType, setProductPickType] = useState({});
  const [errorValidate, setErrorValidate] = useState({});
  const dispatch = useDispatch();
  const product = useSelector((state) => state.product);
  const [openCalendar, setOpenCalendar] = useState({ status: false });
  const [showImg, setShowImg] = useState("");
  const [searchBar, setSearchBar] = useState("");
  const [pickQuantity, setPickQuantity] = useState(1);
  const cart = useSelector((state) => state.cart);
  let history = useHistory();
  const [alertDialog, setAlertDialog] = useState({
    isOpen: false,
    title: "",
  });

  let error = [];
  let formIsValid = true;

  useEffect(() => {
    dispatch(GetProductByName(props.match.params.product));
  }, []);

  useEffect(() => {
    setPickQuantity(1);
  }, [productPickType]);

  const HandleValidation = () => {
    if (
      productPickType.hasOwnProperty("mainType") &&
      productPickType.hasOwnProperty("subType")
    ) {
      if (!productPickType?.mainType) {
        error = !error?.productType
          ? { ...error, productType: "กรุณาเลือกตัวเลือกประเภทของสินค้าก่อน" }
          : error;
        formIsValid = false;
      }
      if (!productPickType?.subType) {
        error = !error?.productType
          ? { ...error, productType: "กรุณาเลือกตัวเลือกประเภทของสินค้าก่อน" }
          : error;
        formIsValid = false;
      }
    } else if (productPickType.hasOwnProperty("mainType")) {
      if (!productPickType?.mainType) {
        error = !error?.productType
          ? { ...error, productType: "กรุณาเลือกตัวเลือกประเภทของสินค้าก่อน" }
          : error;
        formIsValid = false;
      }
    }
    setErrorValidate(error);
  };

  const handleOrder = (dates) => {
    let orderItem = {
      id: product.products.id,
      code: product.products.code,
      name: product.products.name,
      quantity: pickQuantity,
      bookingDates: dates,
    };
    if (productPickType.mainType && productPickType.subType) {
      orderItem = {
        ...orderItem,
        price: productPickType.price,
        picture: { src: productPickType.picture },
        productPickType: {
          mainType: productPickType.mainType,
          subType: productPickType.subType,
        },
      };
    } else if (productPickType.mainType) {
      orderItem = {
        ...orderItem,
        picture: { src: productPickType.picture },
        price: productPickType.price,
        productPickType: {
          mainType: productPickType.mainType,
        },
      };
    } else {
      orderItem = {
        ...orderItem,
        picture: { src: product.products.picture[0].src },
        price: product.products.price,
      };
    }

    const payload = {
      orderItems: [orderItem],
    };

    dispatch(CreateOrder(payload)).then((err) => {
      if (!err) {
        history.push("/orderSuccess");
        return;
      }
      setAlertDialog({
        isOpen: true,
        title: err.message,
      });
      dispatch(GetProductByName(props.match.params.product));
    });
  };

  const handleSaveProduct = async () => {
    await dispatch(isLoggedBeforeAction()).then((err) => {
      if (err) {
        return;
      }
      HandleValidation();
      if (formIsValid) {
        // setOpenCalendar(true);
        setOpenCalendar({
          status: true,
          nextFunction: (dates) => handleOrder(dates),
        });
      }
    });
    // HandleValidation();
    // if (formIsValid) {
    //   // setOpenCalendar(true);
    //   setOpenCalendar({
    //     status: true,
    //     nextFunction: (dates) => handleOrder(dates),
    //   });
    // }
  };

  const handleAddItemToCart = (dates) => {
    let payload = {
      id: product.products.id,
      code: product.products.code,
      name: product.products.name,
      quantity: pickQuantity,
      bookingDates: dates,
    };
    if (productPickType.mainType && productPickType.subType) {
      payload = {
        ...payload,
        price: productPickType.price,
        picture: { src: productPickType.picture },
        productPickType: {
          mainType: productPickType.mainType,
          subType: productPickType.subType,
        },
      };
    } else if (productPickType.mainType) {
      payload = {
        ...payload,
        picture: { src: productPickType.picture },
        price: productPickType.price,
        productPickType: {
          mainType: productPickType.mainType,
        },
      };
    } else {
      payload = {
        ...payload,
        picture: { src: product.products.picture[0].src },
        price: product.products.price,
      };
    }

    dispatch(AddItemToCart(payload)).then((err) => {
      if (err) {
        setAlertDialog({
          isOpen: true,
          title: APP_BAD_REQUEST[err.code],
        });
        dispatch(GetProductByName(props.match.params.product));
      }
    });
  };

  const handleOpenPickDateToCart = async () => {
    await dispatch(isLoggedBeforeAction()).then((err) => {
      if (err) {
        return;
      }
      HandleValidation();
      if (formIsValid) {
        setOpenCalendar({
          status: true,
          nextFunction: (dates) => handleAddItemToCart(dates),
        });
      }
    });
    // HandleValidation();
    // if (formIsValid) {
    //   setOpenCalendar({
    //     status: true,
    //     nextFunction: (dates) => handleAddItemToCart(dates),
    //   });
    // }
  };

  const handleSearchBar = () => {
    window.location.href = `/product?page=1&search=${searchBar}`;
  };

  const handleSearchBarEnterPress = (e) => {
    if (e.key === "Enter") {
      window.location.href = `/product?page=1&search=${searchBar}`;
    }
  };

  const handleIncresePickType = () => {
    const quantity = productPickType.quantity || product.products.quantity;
    if (pickQuantity < quantity) {
      setPickQuantity(pickQuantity + 1);
    }
  };

  const handleDecresePickType = () => {
    if (pickQuantity > 1) {
      setPickQuantity(pickQuantity - 1);
    }
  };

  const handleChangePickQuantity = (e) => {
    const qtyInput = e.target.value;
    const totalQty = productPickType.quantity || product.products.quantity;
    if (qtyInput === "0") {
      return;
    }
    if (qtyInput > totalQty) {
      setPickQuantity(totalQty);
      return;
    }
    setPickQuantity(qtyInput);
  };

  const hnadleClickOutside = (e) => {
    const qtyInput = e.target.value;
    if (qtyInput) {
      return;
    }

    setPickQuantity(1);
  };

  const handleKeyDown = (e) => {
    if ((e.which !== 8 && e.which < 96) || e.which > 105) {
      e.preventDefault();
    }
  };

  return (
    <Layout
      searchBar
      value={searchBar}
      onChange={(e) => setSearchBar(e.target.value)}
      onClick={handleSearchBar}
      onKeyPress={handleSearchBarEnterPress}
    >
      <LoadingController />
      <AlertWarning alertDialog={alertDialog} setAlertDialog={setAlertDialog} />
      <GlobalStyle color={1} />
      {product.loading ? (
        <Loader />
      ) : (
        <Container>
          <ProductDetailContent>
            {product.products.picture ? (
              <Slideshow
                imgs={product.products.picture.map((pic) => pic.src)}
                showImg={showImg}
                setShowImg={setShowImg}
              />
            ) : null}
            <div className="content-2">
              <h2 className="headerNameProduct">{product.products?.name}</h2>
              <div className="categoryProduct">
                ประเภท: {product.products.category?.name}
              </div>
              <div className="priceProduct">
                ราคา: {productPickType.price || product.products?.price}
              </div>
              {product.products.productPickType ? (
                <ProductTypePicker
                  setProductPickType={setProductPickType}
                  productPickType={productPickType}
                  data={product.products.productPickType}
                  setShowImg={setShowImg}
                />
              ) : null}
              <div className="quantityItem">
                <div className="addQuantity">
                  <button onClick={handleDecresePickType}>-</button>
                  <input
                    className="inputQnatity"
                    type="number"
                    value={pickQuantity}
                    onChange={handleChangePickQuantity}
                    onBlur={hnadleClickOutside}
                    onKeyDown={handleKeyDown}
                  ></input>
                  <button onClick={handleIncresePickType}>+</button>
                </div>
                <div className="quantityProduct">
                  มีให้เช่าทั้งหมด:
                  {productPickType.quantity || product.products.quantity}
                </div>
              </div>
              <div className="hireButton">
                <div style={{ color: "red" }}>
                  {errorValidate?.productType
                    ? errorValidate.productType
                    : null}
                </div>
                <button onClick={handleSaveProduct} className="button1">
                  เช่า
                </button>
                <button onClick={handleOpenPickDateToCart} className="button2">
                  หยิบใส่ตะกร้า
                </button>
                <CalendarBooking
                  setProductPickType={setProductPickType}
                  productPickType={productPickType}
                  data={product.products.disableDate}
                  setOpenCalendar={setOpenCalendar}
                  openCalendar={openCalendar}
                  defalultQuantity={product.products.quantity}
                  pickQuantity={pickQuantity}
                />
              </div>
            </div>
          </ProductDetailContent>
        </Container>
      )}
    </Layout>
  );
}

export default ProductDetail;
