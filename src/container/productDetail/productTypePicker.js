// import React, { useState, useEffect } from "react";

// function ProductTypePicker({ data, setProductPickType, productPickType }) {

//     useEffect(() => {
//         const initialProductPickType = data.map((index) => ({
//           nameType: index.nameType,
//           productType: "",
//         }));

//         setProductPickType(initialProductPickType);
//       }, []);

//       const handlePickType = (type, index) => {
//         const newProductPickType = [...productPickType];
//         if (newProductPickType[index].productType === type.type) {
//           newProductPickType[index].productType = "";
//         } else {
//           newProductPickType[index].productType = type.type;
//         }

//         setProductPickType(newProductPickType);
//     };

//     return (
//         <>
//         {data.map((index, i) => (
//           <div className="typePickProduct" key={i}>
//             <div className="nameProduct">{index.nameType}</div>
//             <div className="typeProduct">
//               {index.productType.map((type, y) => (
//                 <button
//                   key={y}
//                   onClick={() => handlePickType(type, i)}
//                   className={
//                     productPickType?.[i].productType === type.type ? "active" : ""
//                   }
//                 >
//                   {type.type}
//                 </button>
//               ))}
//             </div>
//           </div>
//         ))}
//       </>
//     )
// }

// export default ProductTypePicker

import React, { useState, useEffect } from "react";

function ProductTypePicker({
  data,
  setProductPickType,
  productPickType,
  setShowImg,
}) {
  const [subType, setSubType] = useState(0);

  useEffect(() => {
    if (!productPickType?.quantity && productPickType?.subType) {
      setProductPickType({ ...productPickType, subType: "" });
    }
  }, [productPickType]);

  useEffect(() => {
    let initialProductPickType = {};
    if (data.productTypes[0]?.subType) {
      initialProductPickType = {
        mainType: "",
        subType: "",
        price: "",
        quantity: "",
        picture: "",
      };
    } else if (data.productTypes.length > 0) {
      initialProductPickType = {
        mainType: "",
        price: "",
        quantity: "",
        picture: "",
      };
    }
    setProductPickType(initialProductPickType);
  }, []);

  const handlePickMainType = (type, index) => {
    const newProductPickType = { ...productPickType };
    if (newProductPickType?.mainType === type.nameType) {
      newProductPickType.mainType = "";
      newProductPickType.price = "";
      newProductPickType.quantity = "";
      newProductPickType.picture = "";
      setSubType(0);
    } else {
      newProductPickType.mainType = type.nameType;
      newProductPickType.price = type.price || "";
      newProductPickType.quantity = type.quantity || "";
      newProductPickType.picture = type?.picture?.src || "";
      setSubType(index);
    }

    setProductPickType(newProductPickType);
  };

  const handlePickSubType = (type, index) => {
    const newProductPickType = { ...productPickType };
    if (newProductPickType?.subType === type.nameType) {
      newProductPickType.subType = "";
      newProductPickType.price = "";
      newProductPickType.quantity = "";
      setSubType(0);
    } else {
      newProductPickType.subType = type.nameType;
      newProductPickType.price = type.price || "";
      newProductPickType.quantity = type.quantity || "";
    }

    setProductPickType(newProductPickType);
  };

  return (
    <>
      <div className="typePickProduct">
        <div className="nameProduct">{data.nameType}</div>
        <div className="typeProduct">
          {data?.productTypes.map((type, y) => (
            <React.Fragment key={y}>
              {type.hasOwnProperty("quantity") && type?.quantity === 0 ? (
                <button key={y} disabled className="disable">
                  {type.nameType}
                </button>
              ) : (
                <button
                  key={y}
                    onClick={() => handlePickMainType(type, y)}
                    onMouseEnter={() =>setShowImg(type?.picture?.src)}
                  className={
                    productPickType?.mainType === type.nameType ? "active" : ""
                  }
                >
                  {type.nameType}
                </button>
              )}
              {/* <button
                key={y}
                onClick={() => handlePickMainType(type, y)}
                className={
                  productPickType?.mainType === type.nameType ? "active" : ""
                }
                disabled={
                  type.hasOwnProperty("quantity")
                    ? type.quantity
                      ? false
                      : true
                    : false
                }
              >
                {type.nameType}
              </button> */}
            </React.Fragment>
          ))}
        </div>
      </div>
      {data.productTypes[subType]?.subType ? (
        <div className="typePickProduct">
          <div className="nameProduct">
            {data.productTypes[subType].subType.nameSubType}
          </div>
          <div className="typeProduct">
            {data.productTypes[subType].subType.productSubTypes.map(
              (type, y) => (
                <React.Fragment key={y}>
                  {type.hasOwnProperty("quantity") && type?.quantity === 0 ? (
                    <button key={y} disabled className="disable">
                      {type.nameType}
                    </button>
                  ) : (
                    <button
                      key={y}
                      onClick={() => handlePickSubType(type, y)}
                      className={
                        productPickType?.subType === type.nameType
                          ? "active"
                          : ""
                      }
                    >
                      {type.nameType}
                    </button>
                  )}
                </React.Fragment>
                // <button
                //   key={y}
                //   onClick={() => handlePickSubType(type, y)}
                //   className={
                //     productPickType?.subType === type.nameType ? "active" : ""
                //   }
                //   disabled={
                //     type.hasOwnProperty("quantity")
                //       ? type.quantity
                //         ? false
                //         : true
                //       : false
                //   }
                // >
                //   {type.nameType}
                // </button>
              )
            )}
          </div>
        </div>
      ) : null}
    </>
  );
}

export default ProductTypePicker;
