import "./App.css";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import Home from "./container/home/home";
// import Grid from "./container/gridTest/grid";
import ProductDetail from "./container/productDetail/productDetail";
// import GridLayoutTest from "./container/gridLayoutTest/gridLayoutTest";
// import Calendar from "./container/calendar/calendar";
import Loader from "./component/loader/loader";
import Signin from "./container/signin/signin";
import { isLogged } from "./action/auth/authAction";
import BasePageNotfound from "./container/basePageNotfound/basePageNotfound";
// import AlertWarning from "./component/alertWarning/alertWarning"
import Cart from "./container/cart/cart";
import OrderSuccess from "./container/orderSuccess/orderSuccess";
import OrderHistory from "./container/orderHistory/orderHistory";
import PrivateRoute from "./component/privateRoute/privateRoute";

function App() {
  const dispatch = useDispatch();
  const auth = useSelector((state) => state.auth);

  useEffect(() => {
    if (!auth.authenticate) {
      dispatch(isLogged());
    }

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [auth.authenticate]);
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/signin" exact component={Signin} />
          <Route path="/" exact component={Home} />
          <Route path="/product" exact component={Home} />
          <PrivateRoute path="/cart" exact component={Cart} />
          <Route path="/orderSuccess" exact component={OrderSuccess} />
          <PrivateRoute path="/order_history" exact component={OrderHistory} />
          {/* <Route path="/alert" component={AlertWarning} /> */}
          <Route path="/product/:category" component={Home} />
          {/* <Route path="/calendar" component={Calendar} /> */}
          <Route path="/:product" component={ProductDetail} />
          {/* <Route path="/grid" exact component={Grid} /> */}
          {/* <Route path="/test/layout" component={GridLayoutTest} /> */}
          {/* <Route path="/calendar" component={Calendar} /> */}
          <Route path="/loader" component={Loader} />
          <Route path="*" component={BasePageNotfound} />
        </Switch>
      </Router>
    </div>
  );
}

export default App;
