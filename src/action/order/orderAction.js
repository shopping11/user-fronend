// import axios from '../../helper/axios'
import { orderType } from './orderType'
import { Config_ls } from '../../utils/config'
import { cartType } from '../cart/cartType'
// import axios from '../../helper/axios'
import axios from 'axios'

export const CreateOrder = (payload) => { 
    return async (dispatch) => { 
        try {
            dispatch({ type: orderType.CREATE_ORDER_REQUEST })
            const token = window.localStorage.getItem(Config_ls.userToken)
            // const res = await axios.post("/carts", payload)
            const res = await axios.post("http://localhost:3003/orders", payload, {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : ''
                }
            })
            // const res = await axios.post('/orders', payload )

            if (res.status === 200) { 
                const data = res.data.data
                dispatch({
                    type: orderType.CREATE_ORDER_SUCCESS
                })
                dispatch({
                    type: cartType.GET_CART_SUCCESS,
                    payload: data
                })
            }
        } catch (err) { 
            if (err.response.status === 400) {
                dispatch({
                    type: orderType.CREATE_ORDER_FAILURE,
                    payload: err.response.data
                })
                return err.response?.data
            }
        }
    }
}

export const GetOrderByUser = (state, payload) => { 
    return async (dispatch) => { 
        try {
            dispatch({ type: orderType.GET_ORDER_HOSTORY_REQUEST })
            const token = window.localStorage.getItem(Config_ls.userToken)
            const res = await axios.post(`http://localhost:3003/orders/order-history?state=${state}`, payload, {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : ''
                }
            })
            // const res = await axios.post(`/orders/order-history?state=${state}`, payload)
            if (res.status === 200) { 
                const { data, filter } = res.data
                dispatch({
                    type: orderType.GET_ORDER_HOSTORY_SUCCESS,
                    payload: { data, filter }
                })
            }
        } catch (err) { 
            if (err.response?.status === 400) {
                dispatch({
                    type: orderType.GET_ORDER_HOSTORY_FAILURE,
                    payload: err.response.data
                })
            }
        }
    }
}

export const CancleOrder = (payload,page) => { 
    return async (dispatch) => { 
        try {
            dispatch({ type: orderType.CANCLE_ORDER_REQUEST })
            const token = window.localStorage.getItem(Config_ls.userToken)
            const res = await axios.patch(`http://localhost:3003/orders/cancle-order-request`, payload, {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : ''
                }
            })
            // const res = await axios.patch('/orders/cancle-order-request', payload)
            if (res.status === 200) { 
                const { search } = window.location
                const state = new URLSearchParams(search).get("state")
                dispatch(GetOrderByUser(state,page))              
                dispatch({
                    type: orderType.CANCLE_ORDER_SUCCESS
                })
            }
        } catch (err) { 
            if (err.response?.status === 400) {
                dispatch({
                    type: orderType.CANCLE_ORDER_FAILURE,
                    payload: err.response.data
                })
            }
        }
    }
}