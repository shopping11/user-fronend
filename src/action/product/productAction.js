import axios from '../../helper/axios'
import { productType } from './productType'


export const GetProduct = (page,search) => {
    return async (dispatch) => { 
        try {
            dispatch({ type: productType.GET_PRODUCT_REQUEST })
            const res = await axios.get(`/products/product/public?page=${page}&search=${search}`)
            if (res.status === 200) {
                const { data, filter } = res.data
                dispatch({
                    type: productType.GET_PRODUCT_SUCCESS,
                    payload: {data,filter}
                })
            }
        } catch (err) {
            if (err.response.status === 400) {
                dispatch({
                    type: productType.GET_PRODUCT_FAILURE,
                    payload: err.response?.data
                })
            }
        }
    }
}

export const GetProductByCategory = (category, page,search) => {
    return async (dispatch) => { 
        try {
            dispatch({ type: productType.GET_PRODUCT_REQUEST })
            const res = await axios.get(`/products/product/public/${category}?page=${page}&search=${search}`)
            if (res.status === 200) {
                const { data, filter } = res.data
                dispatch({
                    type: productType.GET_PRODUCT_SUCCESS,
                    payload: {data,filter}
                })
            }
        } catch (err) {
            if (err.response.status === 400) {
                dispatch({
                    type: productType.GET_PRODUCT_FAILURE,
                    payload: err.response?.data
                })
            }
        }
    }
}

export const GetProductByName = (name) => {
    return async (dispatch) => { 
        try {
            dispatch({ type: productType.GET_PRODUCT_BY_NAME_REQUEST })
            const res = await axios.get(`products/product/${name}`)
            if (res.status === 200) {
                const { data } = res.data
                dispatch({
                    type: productType.GET_PRODUCT_BY_NAME_SUCCESS,
                    payload: { data }
                })
            }
        } catch (err) {
            if (err.response?.status === 400) {
                dispatch({
                    type: productType.GET_PRODUCT_BY_NAME_FAILURE,
                    payload: err.response.data
                })
            } else if(err.response?.status === 404) { 
                    window.location.href = "/"
            }
        }
    }
}

export const ClearError = () => {
    return async dispatch => {
        dispatch({ type: productType.PRODUCT_CLEAR_ERROR })
    }
}