import axios from '../../helper/axios'
import { categoryType } from './categoryType'


export const GetCategory = () => {
    return async (dispatch) => { 
        try {
            dispatch({ type: categoryType.GET_CATEGORY_REQUEST })
            const res = await axios.get("/products/category/front")
            if (res.status === 200) {
                const data = res.data.data
                dispatch({
                    type: categoryType.GET_CATEGORY_SUCCESS,
                    payload: data
                })
            }
        } catch (err) {
            if (err.response?.status === 400) {
                dispatch({
                    type: categoryType.GET_CATEGORY_FAILURE,
                    payload: err.response.data
                })
            }
        }
    }
}

export const ClearError = () => {
    return async dispatch => {
        dispatch({ type: categoryType.CATEGORY_CLEAR_ERROR })
    }
}