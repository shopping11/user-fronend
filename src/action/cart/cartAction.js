// import axios from '../../helper/axios'
import { cartType } from './cartType'
import axios from 'axios'
import { Config_ls } from '../../utils/config'

export const AddItemToCart = (payload) => { 
    return async (dispatch) => { 
        try {
            dispatch({ type: cartType.ADD_CART_REQUEST })
            const token = window.localStorage.getItem(Config_ls.userToken)
            // const res = await axios.post("/carts", payload)
            const res = await axios.post("http://localhost:3003/carts", payload, {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : ''
                }
            })
            if (res.status === 200) { 
                const data = res.data.data
                dispatch({
                    type: cartType.ADD_CART_SUCCESS,
                    payload: data
                })
            }
        } catch (err) { 
            if (err.response.status === 400) {
                dispatch({
                    type: cartType.ADD_CART_FAILURE,
                    payload: err.response.data
                })
                return err.response?.data
            }
        }
    }
}

export const GetCart = () => { 
    return async (dispatch) => { 
        try {
            dispatch({ type: cartType.GET_CART_REQUEST })
            const token = window.localStorage.getItem(Config_ls.userToken)
            // const res = await axios.post("/carts", payload)
            const res = await axios.get("http://localhost:3003/carts", {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : ''
                }
            })
            if (res.status === 200) { 
                const data = res.data.data
                dispatch({
                    type: cartType.GET_CART_SUCCESS,
                    payload: data
                })
            }
        } catch (err) { 
            if (err.response?.status === 400) {
                dispatch({
                    type: cartType.DELETE_CART_FAILURE,
                    payload: err.response.data
                })
            }
        }
    }
}

export const DeleteItemInCart = (payload) => { 
    return async (dispatch) => { 
        try {
            dispatch({ type: cartType.DELETE_CART_REQUEST })
            const token = window.localStorage.getItem(Config_ls.userToken)
            // const res = await axios.post("/carts", payload)
            const res = await axios.patch("http://localhost:3003/carts", payload, {
                headers: {
                    'Authorization': token ? `Bearer ${token}` : ''
                }
            })
            if (res.status === 200) { 
                const data = res.data.data
                dispatch({
                    type: cartType.DELETE_CART_SUCCESS,
                    payload: data
                })
            }
        } catch (err) { 
            if (err.response.status === 400) {
                dispatch({
                    type: cartType.DELETE_CART_FAILURE,
                    payload: err.response?.data
                })
            }
        }
    }
}