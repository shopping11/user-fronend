export const cartType = {
  GET_CART_REQUEST: "GET_CART_REQUEST",
  GET_CART_SUCCESS: "GET_CART_SUCCESS",
  GET_CART_FAILURE: "GET_CART_FAILURE",

  ADD_CART_REQUEST: "ADD_CART_REQUEST",
  ADD_CART_SUCCESS: "ADD_CART_SUCCESS",
  ADD_CART_FAILURE: "ADD_CART_FAILURE",

  DELETE_CART_REQUEST: "DELETE_CART_REQUEST",
  DELETE_CART_SUCCESS: "DELETE_CART_SUCCESS",
  DELETE_CART_FAILURE: "DELETE_CART_FAILURE",
};
